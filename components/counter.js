import React from "react"
import "isomorphic-unfetch"

class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: props.initialValue
    };
  }

  async handleClick() {
    const res = await fetch(`http://${this.props.host}/api/counter`, {
      method: "POST",
      body: {
        counter: this.state.counter
      }
    })

    if (res.status == 200) {
      const {counter} = await res.json()
      this.setState({ counter })
    }
    
  }

  render() {
    return (
      <>
        <button onClick={this.handleClick.bind(this)}>Increment Counter</button>
        <div>Count: {this.state.counter}</div>
      </>
    )
  }
}

export default Counter
