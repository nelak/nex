const express = require('express')
const next = require('next')
const dataAccess = require('./shared/dataAccess')

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

app.prepare().then(() => {
  const server = express()

  server.get('/api/counter', async (req, res) => {
    try {
      res.json(await dataAccess({ collection: 'counter'}));
    } catch (error) {
      console.error(error)
      return res.sendStatus(500)
    }
  })

  server.post('/api/counter', async (req, res) => {
    try {
      return res.json(await dataAccess({ collection: 'counter', increment:true}))
    } catch (error) {
      console.error(error)
      return res.sendStatus(500)
    }
  })

  server.get('*', (req, res) => {
    return handle(req, res)
  })

  server.listen(port, err => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
})
