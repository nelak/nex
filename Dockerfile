# Do the npm install or yarn install in the full image
FROM node:latest
WORKDIR /app
# Copy the current directory contents into the container at /app
ADD . /app
RUN yarn install
RUN yarn build

# And then copy over node_modules, etc from that stage to the smaller base image
FROM mhart/alpine-node:latest
COPY --from=0 /app /app
WORKDIR /app
EXPOSE 3000
CMD ["yarn", "start"]