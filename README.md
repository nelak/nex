
# Node

## Run development

`yarn dev`

## Run production

```bash
yarn build
yarn start
```

# Docker

## Build

`docker-compose build`

## Run

`docker-compose up`