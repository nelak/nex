const MongoClient = require('mongodb').MongoClient;
const util = require('util')

// Connection url
const url = process.env.CONNECTION_STRING || 'mongodb://localhost:27017'

async function incrementCounter({database = 'default', collection, increment}) {

    const connect = util.promisify(MongoClient.connect)

    const client = await connect(`${url}/${database}`, { useNewUrlParser: true})
        
    // Use the admin database for the operation
    const counter = client.db(database).collection(collection);

    let result;

    if(!increment) {
        result = await counter.findOne() || {counter: 0}
    } else {
        await counter.update({}, {$inc: { counter: 1}}, {upsert: true})
        result = await counter.findOne()
    }

    // List all the available databases
    client.close()

    return result
}

module.exports = incrementCounter;