import React from 'react'
import Counter from '../components/counter'

const Index = ({initialValue, host}) => (
  <Counter initialValue={initialValue} host={host}></Counter>
)

Index.getInitialProps = async ({ req }) => {
  const res = await fetch(`http://localhost:3000/api/counter`)
  const json = await res.json()
  return { 
    initialValue: json.counter,
    host: req.headers.host
  }
}

export default Index